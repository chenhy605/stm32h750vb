#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
    int f_src, f_dst;
    int read_size;
    unsigned char buf[4096];
    f_src = open("decmprs_boot.bin", O_RDWR);
    remove("boot.bin");
    f_dst = open("boot.bin", O_RDWR|O_CREAT|O_SYNC, S_IRUSR|S_IWUSR);

    while(1)
    {
        read_size = read(f_src, buf, 4096);
        if(read_size)
            write(f_dst, buf, read_size);
        else
            break;
    }
    close(f_src);

    lseek(f_dst, 4*1024, SEEK_SET);

    f_src = open("minilzo.lzo", O_RDWR);
    while(1)
    {
        read_size = read(f_src, buf, 4096);
        if(read_size)
            write(f_dst, buf, read_size);
        else
            break;
    }
    close(f_src);
    close(f_dst);
    return 0;
}
