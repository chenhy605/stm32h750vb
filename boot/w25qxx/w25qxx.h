#ifndef __W25QXX_H__
#define __W25QXX_H__

#define WRITE_ENABLE        0x06
#define WRITE_ENABLE_VSR    0x50
#define WRITE_DISABLE       0x04
#define READ_SR1            0x05
#define READ_SR2            0x35
#define READ_SR3            0x15
#define WRITE_SR1           0x01
#define WRITE_SR2           0x31
#define WRITE_SR3           0x11

#define READ_DATA           0x03
#define PAGE_PROGRAM        0x02

#define FAST_READ_QUAD      0xEB

#define SECTOR_ERASE        0x20
#define BLOCK_ERASE_32K     0x52
#define BLOCK_ERASE_64K     0xD8
#define CHIP_ERASE          0xC7
#define READ_DEVICE_ID      0x90

#define ENABLE_RESET        0x66
#define RESET_DEVICE        0x99

#define W25QXX_PAGE_SIZE    256
#define W25QXX_SECTOR_SIZE  4096

unsigned short w25qxx_qspi_id(void);
int w25qxx_qspi_init(void);
int w25qxx_qspi_reset(void);
int w25qxx_qspi_erase(unsigned int address);
int w25qxx_qspi_read(unsigned int address, void *pbuf, int size);
int w25qxx_qspi_write(unsigned int address, void *pbuf, int size);
int w25qxx_qspi_mem_mapped(void);

unsigned short w25qxx_spi_id(void);
int w25qxx_spi_init(void);
int w25qxx_spi_reset(void);
int w25qxx_spi_erase(unsigned int address);
int w25qxx_spi_read(unsigned int address, void *pbuf, int size);
int w25qxx_spi_write(unsigned int address, void *pbuf, int size);
int w25qxx_spi_mem_mapped(void);
#endif // !__W25QXX_H__
